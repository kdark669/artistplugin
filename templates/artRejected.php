<?php
global $wpdb;
$table_name = $wpdb->prefix . 'art_itg';
$results = $wpdb->get_results("SELECT * FROM $table_name where delete_flag = 1");
if (empty($results)) {
    ?>
    <p>No Arts </p>
    <?php
}
else {
    ?>
    <div class="artContainer">
        <div class="updatedSuccess notice success" id="updatedSuccess">
        </div>
        <div class="errorR notice" id="errorR">
        </div>
        <?php
        foreach ($results as $row) {
            $image = $row->image;
            $plugin_dir = '/wp-content/plugins/Artist/Uploads/';
            $artist_detail = get_userdata($row->artist_id);
            ?>

            <div class="uploadedArt">
                <div class="updatedSuccess notice success" id="updatedSuccess">
                </div>
                <div class="errorR notice" id="errorR">
                </div>
                <?php if ($row->accepted_flag == 0) { ?>
                    <div class="adminAction" id="adminAction">
                        <div class="card" style="width: 18rem;">
                            <div class="upload-images">
                                <img class="card-img-top" src="<?php echo $plugin_dir . $image ?>"
                                     alt="artist image">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo strtoupper($row->name)?></h5>
                                <span style="font-weight: bold;">Uploaded By: </span><?php echo $artist_detail->user_nicename ?>
                                <p class="card-text"><?php echo $row->description ?></p>
                                <form action="" id="recoverArt" class="recoverArt" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="image_id" value="<?php echo $row->id ?>">
                                    <input type="hidden" name="user_id" value="<?php echo $artist_detail->ID ?>">
                                    <button class="btn btn-accept" type="submit" name="accept" >Recover</button>
                                </form>
                                <form action="" method="post" id="deleteArt" class="deleteArt">
                                    <input type="hidden" name="image_id" value="<?php echo $row->id ?>">
                                    <input type="hidden" name="user_id" value="<?php echo $artist_detail->ID ?>">
                                    <button class="btn btn-reject" type="submit" name="reject">Permanent Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } else {?>

                    <div class="adminAction">
                        <span>Accepted</span>
                    </div>
                <?php } ?>
            </div>
            <?php

        } ?>
    </div>
    <?php
}