<?php
global $wpdb;
$table_name = $wpdb->prefix . 'art_itg';
$results = $wpdb->get_results("SELECT * FROM $table_name where accepted_flag = 0");
if (empty($results)) {
    ?>
    <p>Art List Empty</p>
    <?php
}
else {
    ?>
    <div class="artContainer">
        <div class="updatedSuccess notice success" id="updatedSuccess">
        </div>
        <div class="errorR notice" id="errorR">
        </div>
        <?php
        foreach ($results as $row) {
            $image = $row->image;
            $plugin_dir = '/wp-content/plugins/Artist/Uploads/';
            $artist_detail = get_userdata($row->artist_id);
            ?>
            <div class="uploadArt">
                <?php if ($row->accepted_flag == 0) { ?>
                    <div class="adminAction" id="adminAction">
                        <div class="card" style="width: 18rem;">
                            <div class="upload-images">
                                <img class="card-img-top" src="<?php echo $plugin_dir . $image ?>"
                                     alt="artist image">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo strtoupper($row->name)?></h5>
                                <span style="font-weight: bold;">Uploaded By: </span><?php echo $artist_detail->user_nicename ?>
                                <p class="card-text"><?php echo $row->description ?></p>
                                <form action="" id="moveArt" class="moveArt" method="post" enctype="multipart/form-data">
                                    <input type="text" name="ImageId" value="<?php echo $row->id ?>" hidden>
                                    <input type="text" name="artistId" value="<?php echo $row->id ?>" hidden>
                                    <input type="text" name="user_Id" value="<?php echo $artist_detail->ID ?>" hidden>
                                    <input type="text" name="artistImage" value="<?php echo $plugin_dir . $image ?>"
                                           hidden>
                                    <button class="btn btn-accept" type="submit" name="accept" > Accept </button>
                                </form>
                                <form action="" method="post" id="rejectArt" class="rejectArt">
                                    <input type="hidden" name="artistImage" value="<?php echo $plugin_dir . $image ?>">
                                    <input type="text" name="imageId" value="<?php echo $row->id ?>" hidden>
                                    <input type="text" name="user_id" value="<?php echo $artist_detail->ID ?>" hidden>
                                    <button class="btn btn-reject" type="submit" name="reject">Reject</button>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
?>

