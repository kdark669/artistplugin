<?php
global $wpdb;
$table_name = $wpdb->prefix . 'art_itg';
$results = $wpdb->get_results("SELECT * FROM $table_name where accepted_flag = 1");
if (empty($results)) {
    ?>
    <p>No Any Artist Uploaded The Arts </p>
    <?php

}
else {
    ?>
    <div class="container">
        <table id="example" class="table mytable table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Image</th>
                <th>Uploaded By</th>
            </tr>
            </thead>
            <?php  foreach ($results as $row) {
                $image = $row->image;
                $plugin_dir = '/wp-content/plugins/Artist/Uploads/';
                $artist_detail = get_userdata($row->artist_id);
            ?>
            <tr>
                <td><img class="upload-images" src="<?php echo $plugin_dir . $image ?>"
                         alt="artist image" style="width: 150px; height: 50px"
                    ></td>
                <td><?php echo $artist_detail->user_nicename ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
    <?php
}