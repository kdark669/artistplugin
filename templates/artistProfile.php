<?php
get_header();
global $wp_query;
$users = wp_get_current_user();
$user_role = ( array )$users->roles;
$post = get_post($users->ID);
$user_id = get_current_user_id();
$plugin_dir = '/wp-content/plugins/Artist/Uploads/';
$pluginurl = plugins_url('Artist');
$avatar_url = get_avatar_url( $user_id );
$attach = $wpdb->get_results(" SELECT * FROM wp_art_itg where artist_id = $user_id and accepted_flag = 1 ");
$avatar = $wpdb->get_results(" SELECT * FROM wp_artistprofile_itg where user_id = $user_id")[0];
if ($user_role[0] == 'artist') {
    ?>

    <div class="container-fluid">
        <div class="profile-card">
            <div class="banner">
                <img src="<?php echo $pluginurl .'/public/assets/img/banner.png'?>" class="banner-img">
            </div>
            <div class="updated notice success" id="uploaded">
            </div>
            <div class="errorR notice" id="errorR">
            </div>
            <div class="profile-info">
                <div class="profile-pic">
                    <?php if(empty($avatar)){
                        ?>
                            <img src="<?php echo $avatar_url ?>">
                        <?php
                    } else { ?>
                    <img src="<?php echo $pluginurl.'/Profiles/'.$avatar->profile ?>">
                    <?php } ?>
                    <form method="post" enctype="multipart/form-data" class="userAvatar" id="userAvatar">
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo get_current_user_id(); ?>">
                        <span class="error" id="euser_avatar" style="color: #e25454"></span>
                        <input type="hidden" name="a_id" id="a_id" value="<?php echo $avatar->id ?>">
                        <input type="file" name="user_avatar" id="user_avatar">
                        <button type="submit"> Change Avatar</button>
                    </form>
                </div>
                <div class="artist-info">
                    <p class="artist-name"><?php echo $users->display_name ?></p>
                    <p class="artist-bio">ARTIST, ENTREPRENEUR, DREAMER</p>
                </div>
            </div>
        </div>

        <div class="Art">
            <form class="uploadArt" id="uploadArt" method="post" enctype="multipart/form-data">
                <label for="artistId">Upload Arts</label>
                <input type="hidden" name="artistId" id="artistId" value="<?php echo get_current_user_id(); ?>">
                <div class="form-group">
                    <input type="file" name="artistImage" id="artistImage" placeholder="Artist Image" onblur="validateImage(event)">
                    <span class="error" id="eartist_file"></span>
                </div>
                <button type="submit">Upload Art</button>
            </form>
        </div>
        <div class="artist-collection">
            <p class="artist-collection-title">YOUR COLLECTIONS</p>
            <div class="artist-collection-items">
                <?php
                if(empty($attach)){
                    ?>
                    <p>Your Collection is Empty</p>
                    <?php
                }
                foreach ( $attach as $a ) {
//                        ?>
                    <div class="card">
                        <img class="card-img-top" src="<?php echo $plugin_dir . $a->image ?>"
                             alt="artist image">
                    </div>
                    <?php
                }

                ?>

            </div>

        </div>
    </div>
    <?php
}
elseif($user_role[0] == 'administrator'){
    ?>
    <p>Greating Admin</p>
    <?php
}else{
    ?>
    <div class="onlyAdmin">
        <p>Would You Like To Be An Artist</p>
        <form action="" method="post">
            <button type='submit' name="beArtist" class="btn"> Yes</button>
            <?php
            if(isset($_POST['beArtist'])){
                $u = new WP_User( $user_id );
                $u->remove_role( 'customer' );
                $u->add_role( 'artist' );
                echo "<script type='text/javascript'>
                window.location=document.location.href;
                </script>";
            }
            ?>
        </form>
    </div>
    <?php
}
get_footer();