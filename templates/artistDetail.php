<?php
get_header();
$user_id = $_GET['aId'];

if(isset($user_id)) {
	$avatar_url = get_avatar_url( $user_id );
	$user       = get_userdata( $user_id );
    $plugin_dir = '/wp-content/plugins/Artist/Uploads/';
    $pluginurl = plugins_url('Artist');
    $attach = $wpdb->get_results(" SELECT * FROM wp_art_itg where artist_id = $user_id and accepted_flag = 1 ");
    $avatar = $wpdb->get_results(" SELECT * FROM wp_artistprofile_itg where user_id = $user_id")[0];
    ?>
	<div class="container-fluid">
		<div class="profile-card">
			<div class="banner">
                <img src="<?php echo $pluginurl .'/public/assets/img/banner.png'?>" class="banner-img">
			</div>
			<div class="profile-info">
				<div class="profile-pic">
                    <?php if(empty($avatar)){
                        ?>
                        <img src="<?php echo $avatar_url ?>">
                        <?php
                    } else { ?>
                        <img src="<?php echo $pluginurl.'/Profiles'.$avatar->profile ?>">
                    <?php } ?>
				</div>
				<div class="artist-info">
					<p class="artist-name"><?php echo $user->display_name ?></p>
					<p class="artist-name"><?php echo $user->nickname ?></p>
					<p class="artist-bio">ARTIST, ENTREPRENEUR, DREAMER</p>
				</div>
			</div>
		</div>
		<div class="artist-collection">
			<p class="artist-collection-title"><?php echo strtoupper( $user->display_name ) ?>'S COLLECTIONS</p>

			<div class="artist-collection-items">
				<?php
				if ( empty( $attach ) ) {
					?>
					<p>The Art Collection is Empty</p>
					<?php
				} else {
                    foreach ( $attach as $a ) {
//                        ?>
                        <div class="card">
                            <img class="card-img-top" src="<?php echo $plugin_dir . $a->image ?>"
                                 alt="artist image">
                        </div>
                        <?php
                    }
				}
				?>
			</div>
		</div>
	</div>

	<?php
}else{
    ?>
    <p>Sorry This Page cannot be access..</p>
    <?php
}
get_footer();
