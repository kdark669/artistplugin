<?php
require_once ('controller/ArtistAdmin.php');
require_once ('controller/MoveArt.php');
require_once ('controller/RejectArt.php');
require_once ('controller/DeleteArt.php');
require_once ('controller/CustomProduct.php');

add_action( 'admin_menu', array( 'ArtistAdmin', 'createArtistMenu' ) );
add_action( 'plugins_loaded', 'wcpt_register_artist_type' );
add_filter( 'product_type_selector', array('CustomProduct','wcpt_add_artist_type'));
add_filter( 'woocommerce_product_data_tabs', array('CustomProduct','artist_tab'));
add_action( 'woocommerce_product_data_panels', array('CustomProduct','wcpt_artist_options_product_tab_content'));
add_action( 'woocommerce_process_product_meta', array('CustomProduct','save_artist_options_field'));

function wcpt_register_artist_type(){
    class WC_Product_Artist extends WC_Product {

        public function __construct( $product ) {
            $this->product_type = 'artist'; // name of your custom product type
            parent::__construct( $product );
            // add additional functions here
        }
    }
}