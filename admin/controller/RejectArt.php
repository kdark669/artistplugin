<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('RejectArt')):

class RejectArt
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }
    public static function handle(){
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) &&$_GET['custom'] == 'rejectArt') {
            global $wpdb;
            $table = $wpdb->prefix . 'art_itg';
	        $data = json_decode(file_get_contents('php://input'), 1);
            $artistId = sanitize_text_field($data['artist_Id']);
	        $user_id = sanitize_text_field($data['user_id']);
	        $user_email = get_userdata( $user_id )->user_email;
            $file = sanitize_text_field($data['artist_file']);
            $data = array(
                'delete_flag' => 1
            );
            $success = $wpdb->update($table, $data,array('id'=>$artistId));
            if($success){
                $subject = 'Art Rejected';
                $to = $user_email;
                $message = '<p>Hi There! We just wanted to let you know your Art <br>
                                <img src="'.$file.'" alt="" style="max-width: 200px; max-height: 300px;">
                                <span style="color: blue; font-size: 10px">'.$file.'</span>
                            <br> has been temporarily rejected.</p>';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: BellyMonk < bellymonk@example.com >');
                wp_mail( $to, $subject, $message, $headers );
                echo json_encode(array('status' => true));
                die();
            }
            else{
                echo json_encode(array('status' => false));
                die();
            }
        }
    }

}
new RejectArt();
endif;