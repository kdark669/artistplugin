<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('DeleteArt')):

    class DeleteArt
    {
        public function __construct()
        {
            add_action( 'init', array( $this, 'handle' ) );
        }
        public static function handle(){
            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) &&$_GET['custom'] == 'permanentDeleteArt') {
                global $wpdb;
                $table = $wpdb->prefix . 'art_itg';
                $data = json_decode(file_get_contents('php://input'), 1);
                $image_id = sanitize_text_field($data['image_id']);
                $success = $wpdb->delete( $table, array( 'id' => $image_id ) );
                if($success){
                    echo json_encode(array('status' => true));
                    die();
                }
                else{
                    echo json_encode(array('status' => false));
                    die();
                }
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) &&$_GET['custom'] == 'recoverArt') {
                global $wpdb;
                $table = $wpdb->prefix . 'art_itg';
                $data = json_decode(file_get_contents('php://input'), 1);
                $image_id = sanitize_text_field($data['image_id']);
                $data = array(
                    'delete_flag' => 0
                );
                $success = $wpdb->update($table, $data,array('id'=>$image_id));
                if($success){
                    echo json_encode(array('status' => true));
                    die();
                }
                else{
                    echo json_encode(array('status' => false));
                    die();
                }
            }

        }

    }
    new DeleteArt();
endif;