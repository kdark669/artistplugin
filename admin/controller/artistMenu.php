<?php


if (!defined('ABSPATH')) {
    die();
}
if (!class_exists('artistMenu')):
class artistMenu
{
    public function __construct()
    {
        $this->plugin_path = plugin_dir_path( dirname( __FILE__, 2 ) );
        $this->plugin_url = plugin_dir_url( dirname( __FILE__, 2 ) );
        $this->plugin = plugin_basename( dirname( __FILE__, 3 ) ) . '/Artist.php';
    }

    public static function artistMenuCallback(){
        wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
        wp_enqueue_style('upload', plugins_url() . '/Artist/admin/assets/css/artist_admin_style.css');
        wp_enqueue_script('moveArt', plugins_url() . '/Artist/admin/assets/js/moveart.js', array(), false, true);
        wp_enqueue_script('rejectArt', plugins_url() . '/Artist/admin/assets/js/Reject.js', array(), false, true);
        return require_once( plugin_dir_path( dirname( __FILE__, 2 )) . "/templates/arts.php" );
    }
    public static function acceptedArtMenuCallback(){
        wp_enqueue_style('table', plugins_url() . '/Artist/admin/assets/css/artist_admin_style.css');
        wp_enqueue_style('upload', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css');
        return require_once( plugin_dir_path( dirname( __FILE__, 2 )) . "/templates/artAceppted.php" );
    }
    public static function rejectedArtMenuCallback(){
        wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
        wp_enqueue_style('upload', plugins_url() . '/Artist/admin/assets/css/artist_admin_style.css');
        wp_enqueue_script('recoverArt', plugins_url() . '/Artist/admin/assets/js/recover.js', array(), false, true);
        wp_enqueue_script('deleteArt', plugins_url() . '/Artist/admin/assets/js/permanentDelete.js', array(), false, true);
        return require_once( plugin_dir_path( dirname( __FILE__, 2 )) . "/templates/artRejected.php" );
    }
}
endif;