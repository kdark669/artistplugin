
<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('MoveArt')):
    class MoveArt
    {
        public function __construct()
        {
            add_action('init', array($this, 'handle'));
        }

        public static function handle()
        {

            if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_GET['custom']) && isset($_GET['moveArt']) && !isset($_GET['register'])){
                global $wpdb;
                $table = $wpdb->prefix . 'art_itg';
                $url = site_url();
                $data = json_decode(file_get_contents('php://input'), 1);
	            $artistId = sanitize_text_field($data['artist_id']);
                $user_id = sanitize_text_field($data['user_id']);
                $file = $url . sanitize_text_field($data['artist_file']);
                $filename = basename($file);
//                $upload_file = wp_upload_bits($filename, null, file_get_contents($file));
//                $image_url = $upload_file['url'];
                $user_email = get_userdata( $user_id )->user_email;
                $data = array(
                    'accepted_flag' => 1
                );
                $success = $wpdb->update($table, $data,array('id'=>$artistId));
                if($success){
                    $subject = 'Art Approved';
                    $to = $user_email;
                    $message = '<p>Hi There! We just wanted to let you know your Art <br>
                                <img src="'.$file.'" alt="" style="max-width: 200px; max-height: 300px;">
                                <span style="color: blue; font-size: 10px">'.$file.'</span>
                            <br> has been accepted and placed to product</p>>';
                    $headers = array('Content-Type: text/html; charset=UTF-8','From: BellyMonk < bellymonk@example.com >');
                    wp_mail( $to, $subject, $message, $headers );
                    echo json_encode(array('status' =>true));
                    die();
                }
                else{
                    echo json_encode(array('status' =>false));
                    die();
                }
            }
        }

    }

    new MoveArt();
endif;