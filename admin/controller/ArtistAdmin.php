<?php

require_once ('artistMenu.php');
if(!class_exists('ArtistAdmin')):
class ArtistAdmin
{
    public static function createArtistMenu(){
        add_menu_page(
            'Art',
            'Arts',
            'administrator',
            'itg_artist',
            array('artistMenu', 'artistMenuCallback'),
            'dashicons-art'
        );
        add_submenu_page(
            'itg_artist',
            'Accepted Arts',
            'Accepted Arts',
            'administrator',
            'accepted_arts',
            array('artistMenu', 'acceptedArtMenuCallback')
        );
        add_submenu_page(
            'itg_artist',
            'Rejected Arts',
            'Rejected Arts',
            'administrator',
            'rejected_arts',
            array('artistMenu', 'rejectedArtMenuCallback')
        );
    }

}
endif;