document.addEventListener('DOMContentLoaded', function (e) {
    let rejectArt = document.getElementsByClassName('rejectArt');
    Array.from(rejectArt).forEach(function (element) {
        element.addEventListener('submit',(e)=>{
            e.preventDefault();
            let data = {
                'artist_Id':e.target['imageId'].value,
                'user_id':e.target['user_id'].value,
                'artist_file': e.target['artistImage'].value,
            };
            let url = window.location.href+'';
            axios.post(url, data,{
                    params:{
                        'custom':'rejectArt'
                    }
                }
                ).then(res => {
                if(res.data.status){
                    var success = document.getElementById('updatedSuccess');
                    success.innerHTML = '<span>Art Deleted SuccssFully</span>';
                    success.style.right = 0;
                    success.style.marginRight = '5px';
                    setTimeout(function () {
                        var success = document.getElementById('updatedSuccess');
                        success.style.display = 'none';
                        var card = document.getElementById('adminAction');
                        card.innerHTML = '';
                    }, 1500);
                }

            }).catch(error => {
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Something went wrong</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })
        })
    });
});