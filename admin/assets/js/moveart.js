document.addEventListener('DOMContentLoaded', function (e) {
    let moveArt = document.getElementsByClassName('moveArt');
    Array.from(moveArt).forEach(function (element) {
        element.addEventListener('submit', (e)=> {
            e.preventDefault();
            let data = {
                'artist_file': e.target['artistImage'].value,
                'artist_id': e.target['artistId'].value,
                'user_id': e.target['user_Id'].value,
            };
            let url = window.location.href+'';
            axios.post(url, data,{
                params:{
                    'moveArt':'moveArt'
                }

            }).then(res => {
                console.log(res.data.status);
                if(res.data.res){
                    e.target['artistImage'].value = '';
                    var success = document.getElementById('updatedSuccess');
                    success.innerHTML = '<span>Art Accepted</span>';
                    success.style.right = 0;
                    success.style.marginRight = '5px';
                    setTimeout(function () {
                        var success = document.getElementById('updatedSuccess');
                        success.style.display = 'none';
                        var card = document.getElementById('adminAction');
                        card.innerHTML = '';
                    }, 1500);
                }
            }).catch(error => {
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Something went wrong</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })

        })
    })
});