<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('ArtistInfo')):
class ArtistInfo
{
	public static function our_artist_info(){
        global $product;
        $p_id = $product->id;

        $post = get_post_meta($p_id);
        $post_author = $post['_selectartist'][0];
        $user_detail = get_userdata($post_author);
        $role = $user_detail->roles[0];
        $artistId = $user_detail->ID;
	    $query_res = get_page_by_title('Artist Details')->ID;
        if($role == 'artist'){
        ?>
        <div class="large-3 columns artprofilelink">
                <span class="some-talent">
                 <span>Art By: </span>
                 <a href="<?php echo site_url() .'/?page_id='.get_page_by_title('Artist Details')->ID.'&aId='.$user_detail->ID?>">
                    <?php echo $user_detail->display_name ?>
                </a>
                </span>
        </div>
        <?php
        }
    }
}
endif;