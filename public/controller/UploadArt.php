<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('UploadArt')):
class UploadArt
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }

    public static function handle(){

            if ($_SERVER["REQUEST_METHOD"] == "POST"  && !isset($_GET['register'])&& !isset($_GET['moveArt']) && isset($_GET['custom']) && $_GET['custom'] == "uploadArt") {
                $data = json_decode(file_get_contents('php://input'), 1);
                $file = $_FILES['artist_file']["tmp_name"];
                $artistId = sanitize_text_field($_POST['artist_id']);
                global $wpdb;
                $table = $wpdb->prefix . 'art_itg';
                $check = getimagesize($file);
                if ($check !== false) {
                    $name =$_FILES['artist_file']["name"];
                    $plugin_dir = ABSPATH . 'wp-content/plugins/Artist/';
                    $target_dir = $plugin_dir . '/Uploads/';
                    $target_file = $target_dir . basename($_FILES["artist_file"]["name"]);
                    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                    $extensions_arr = array("jpg", "jpeg", "png", "gif");
                    $fileType = $_FILES['artist_file']['type'];
                    if($fileType == 'image/png'){
                        if (in_array($imageFileType, $extensions_arr)) {
                            $data = array(
                                'artist_id' => $artistId,
                                'image' => $name,
                                'created' => date("Y-m-d H:i:s"),
                            );
                            move_uploaded_file($file, $target_dir . $name);
                            $success = $wpdb->insert($table, $data);
                            if ($success) {
                                echo json_encode(array('status' => true,'msg' => 'Uploaded Successfully'));
                                die();
                            } else {
                                echo json_encode(array('status' => false));
                                die();
                            }
                        }
                    }else{
                        echo json_encode(array('status' => false,'msg'=>'File Type Error!! Required png image'));
                        die();
                    }
                    echo json_encode(array('error' => true));
                    die();

                }
            }
    }
}

new UploadArt();
endif;