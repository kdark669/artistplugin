<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('AddArtist')):
class AddArtist
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }

    public static function handle()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST'  && !isset($_GET['custom']) && !isset($_GET['moveArt']) && isset($_GET['register'])){
            $data = json_decode(file_get_contents('php://input'), 1);
            require_once(ABSPATH . WPINC . '/registration.php');
            $new_user_id = array(
                'user_login'		=> sanitize_text_field($data['user_login']),
                'user_pass'	 		=> sanitize_text_field($data['user_pass']),
                'user_email'		=> sanitize_text_field($data['user_email']),
                'first_name'		=> sanitize_text_field($data['first_name']),
                'last_name'			=> sanitize_text_field($data['last_name']),
                'user_registered'	=> date('Y-m-d H:i:s'),
                'role'				=> 'artist'
            );

            $exists_email = email_exists( $new_user_id['user_email'] );
            $exists_username = username_exists(  $new_user_id['user_login'] );

            if ( $exists_email || $exists_username) {
                $response = [
                    "status"=>false,
                    "msg"=>"Duplicate entry",
                ];
                echo json_encode($response);
                die();
            }else{
                $user_id = wp_insert_user( $new_user_id ) ;
                wp_new_user_notification($user_id);
                $subject = 'Login Details';
                $to = $new_user_id['user_email'];
                $message = ' <p> Hi There! Welcome To Bellymonk. 
                            We just wanted to let you know your user account on 
                            Bellymonk.com as Artist has been approved.</p><br>
                            <span>Username:'.$new_user_id['user_login'].'</span>
                            <br> <span>'.site_url().'</span>';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: BellyMonk < bellymonk@example.com >');
                wp_mail( $to, $subject, $message, $headers );
                $response = [
                    "status"=>true,
                    "msg"=>" User Added Successfully",
                ];
                echo json_encode($response);
                die();
            }
            exit();
        }
    }

    public static function add_login_Link(){
        $query_res = get_page_by_title('Artists')->ID;
        if(!is_user_logged_in()) {
            ?>
            <a href="<?php echo site_url() .'/?page_id='.get_page_by_title('Artists')->ID?>">
                Become An Artist
            </a>
            <?php
        }
    }
}
new AddArtist();
endif;
