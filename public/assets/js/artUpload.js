function validateImage(e){
    var image = document.getElementById('artistImage').value;
        var eartist_file = document.getElementById('eartist_file');
        eartist_file.style.display = 'none';
        activeFlag = true;
}
document.addEventListener('DOMContentLoaded', function (e) {
    let uploadForm = document.getElementById('uploadArt');
    uploadForm.addEventListener('submit', (e) =>{
        e.preventDefault();
        let activeFlag = true;
        if(!e.target['artistImage'].files[0]){
            var eartist_file = document.getElementById('eartist_file');
            eartist_file.innerHTML = 'Art is Required';
            eartist_file.style.display = 'block';
            activeFlag = false;
        }
        let data = {
        };
        data =  new FormData();
        data.append('artist_file', e.target['artistImage'].files[0]);
        data.append('artist_id', e.target['artistId'].value);
        let url = window.location.href+'';
        if (activeFlag) {
            axios.post(url, data ,{
                headers: {
                    'content-type': 'multipart/form-data' // do not forget this
                },
                params:{
                    'custom':'uploadArt'
                }
            })
                .then(res => {
                    if(res.data.status){
                        e.target['artistImage'].value = '';
                        msg = res.data.msg;
                        var success = document.getElementById('uploaded');
                        success.innerHTML = msg;
                        success.style.right = 0;
                        success.style.marginRight = '5px';
                        setTimeout(function () {
                            var success = document.getElementById('uploaded');
                            success.style.display = 'none';
                        }, 2000);
                    }else{
                        console.log(res.data.msg);
                        msg = res.data.msg;
                        var error = document.getElementById('errorR');
                        error.innerHTML = msg;
                        error.style.right = 0;
                        error.style.marginRight = '5px';
                        setTimeout(function () {
                            var error = document.getElementById('errorR');
                            error.style.display = 'none';
                        }, 2000);
                    }
                })
                .catch(error => {
                    console.log(error);
                    var errorR = document.getElementById('errorR');
                    errorR.innerHTML = '<span>Something went wrong</span>';
                    errorR.style.right = 0;
                    errorR.style.marginRight = '5px';
                    setTimeout(function () {
                        var errorR = document.getElementById('errorR');
                        errorR.style.display = 'none';
                    }, 2000);
                })
        }
    })
});