document.addEventListener('DOMContentLoaded', function (e) {
    let userAvatar = document.getElementById('userAvatar');
    userAvatar.addEventListener('submit', (e) =>{
        e.preventDefault();
        let activeFlag = true;
        console.log('hello upload avatar');
        if(!e.target['user_avatar'].files[0]){
            var euser_avatar = document.getElementById('euser_avatar');
            euser_avatar.innerHTML = 'Please Select Avatar';
            euser_avatar.style.display = 'block';
            activeFlag = false;

        }
        let data = {};
        data =  new FormData();
        data.append('user_avatar', e.target['user_avatar'].files[0]);
        data.append('user_id', e.target['user_id'].value);
        data.append('a_id', e.target['a_id'].value);
        let url = window.location.href+'';
        if(activeFlag){
            axios.post(url, data ,{
                headers: {
                    'content-type': 'multipart/form-data' // do not forget this
                },
                params:{
                    'custom':'changeAvatar'
                }
            }).then(res => {
                if(res){
                    if(res){
                        var success = document.getElementById('uploaded');
                        success.innerHTML = '<span>Artist Profile Changed</span>';
                        success.style.right = 0;
                        success.style.marginRight = '5px';
                        setTimeout(function () {
                            var success = document.getElementById('uploaded');
                            success.style.display = 'none';
                            location.reload();
                        }, 2500);
                    }else{
                        var errorR = document.getElementById('errorR');
                        errorR.innerHTML = '<span>Fail to Create Artist</span>';
                        errorR.style.right = 0;
                        errorR.style.marginRight = '5px';
                        setTimeout(function () {
                            var errorR = document.getElementById('errorR');
                            errorR.style.display = 'none';
                        }, 2000);
                    }
                }
            }).catch(error => {
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Fail to Create Artist</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })
        }
    });
});