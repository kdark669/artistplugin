let activeFlag = true;
function validateUsername() {
    var username = document.getElementById('user_login').value;
    if (!username) {
        var euser_login = document.getElementById('euser_login');
        euser_login.innerHTML = 'UserName is Required';
        euser_login.style.display = 'block';
        activeFlag = false;
    }else{
        var euser_login = document.getElementById('euser_login');
        euser_login.style.display = 'none';
        activeFlag = true;
    }
}

function validateFirstname(e) {
    var firstname = document.getElementById('first_name').value;
    if (!firstname) {
        var efirst_name = document.getElementById('efirst_name');
        efirst_name.innerHTML = 'FirstName is Required';
        efirst_name.style.display = 'block';
        activeFlag = false;
    }else{
        var efirst_name = document.getElementById('efirst_name');
        efirst_name.style.display = 'none';
        activeFlag = true;
    }
}

function validateLastname(e) {
    var lastName = document.getElementById('last_name').value;
    if (!lastName) {
        var elast_name = document.getElementById('elast_name');
        elast_name.innerHTML = 'LastName is Required';
        elast_name.style.display = 'block';
        activeFlag = false;
    }else{
        var elast_name = document.getElementById('elast_name');
        elast_name.style.display = 'none';
        activeFlag = true;
    }
}
function validateEmail(e) {
    var email = document.getElementById('user_email').value;
    if (!email) {
        var euser_email = document.getElementById('euser_email');
        euser_email.innerHTML = 'Email is Required';
        euser_email.style.display = 'block';
        activeFlag = true;
    }else{
        var euser_email = document.getElementById('euser_email');
        euser_email.style.display = 'none';
        activeFlag = false;
    }
}
function validatePassword(e) {
    var password = document.getElementById('user_pass').value;
    if (!password) {
        var euser_pass = document.getElementById('euser_pass');
        euser_pass.innerHTML = 'Password is Required';
        euser_pass.style.display = 'block';
        activeFlag = false;
    }else{
        var euser_pass = document.getElementById('euser_pass');
        euser_pass.style.display = 'none';
        activeFlag = true;
    }
}
function validateRepassword(e) {
    var repassword = document.getElementById('user_repass').value;
    if (!repassword) {
        var euser_repass = document.getElementById('euser_repass');
        euser_repass.innerHTML = 'Confirm Password  is Required';
        euser_repass.style.display = 'block';
        activeFlag = false;
    }else{
        var euser_repass = document.getElementById('euser_repass');
        euser_repass.style.display = 'none';
        activeFlag = true;
    }
}

document.addEventListener('DOMContentLoaded', function (e) {
    let registerForm = document.getElementById('form');
    registerForm.addEventListener('submit', (e) => {
        e.preventDefault();
        let data = {
            user_login: e.target['user_login'].value,
            first_name: e.target['first_name'].value,
            last_name: e.target['last_name'].value,
            user_email: e.target['user_email'].value,
            user_pass: e.target['user_pass'].value,
            user_repass: e.target['user_repass'].value,
            url: e.target['url'].value,
        };
        validateUsername();
        validateFirstname();
        validateLastname();
        validateEmail();
        validatePassword();
        validateRepassword();

        if (data.user_pass !== data.user_repass) {
            var errorPassword = document.getElementById('errorPassword');
            errorPassword.innerText = 'Password and Confirm Password are not Same';
            errorPassword.style.display = 'block';
            errorPassword.style.color = 'red';
            activeFlag = false;
        }

        let url = window.location.href+ '';
        console.log(url);
        if (activeFlag) {
            axios.post(
                url, data,{
                    params:{
                        'register':'registerArtist'
                    }
                }
            ).then(res => {
                console.log(res.data.status);
                if (res.data.status) {
                    console.log('i am res is true');
                    e.target['user_login'].value = '';
                    e.target['first_name'].value = '';
                    e.target['last_name'].value = '';
                    e.target['user_email'].value = '';
                    e.target['user_pass'].value = '';
                    e.target['user_repass'].value = '';
                    var success = document.getElementById('updated');
                    success.innerHTML = '<span>Welcome To BellyMonk. Please Login</span>';
                    success.style.right = 0;
                    success.style.marginRight = '5px';
                    setTimeout(function () {
                        var success = document.getElementById('updated');
                        success.style.display = 'none';
                        window.location=data.url;
                    }, 2500);
                }
                else {
                    var errorR = document.getElementById('errorR');
                    errorR.innerHTML = '<span>'+res.data.msg+'</span>';
                    errorR.style.right = 0;
                    errorR.style.marginRight = '5px';
                    setTimeout(function () {
                        var errorR = document.getElementById('errorR');
                        errorR.style.display = 'none';
                    }, 2000);
                }
            }).catch(error => {
                // var errorR = document.getElementById('errorR');
                // errorR.innerHTML = '<span>Internal working error</span>';
                // errorR.style.right = 0;
                // errorR.style.marginRight = '5px';
                // setTimeout(function () {
                //     var errorR = document.getElementById('errorR');
                //     errorR.style.display = 'none';
                // }, 2000);
            })
        }
    })
});